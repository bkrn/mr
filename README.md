# mr

A simple tool for storing and getting strings to and from disk using FIFO or LIFO strategies on the command line

## Usage

[//]: # (TEST)
```sh
# assuming mr starts empty
mr hello                   # put a value into mr
mr "this is"               # put a value into mr
mr "the mr"                # put a value into mr
mr -p -command line tool # put a value into mr
mr -f                      # writes least recent put value to std out
hello                      # response -- 0 -- printed to std out
mr -l                      # writes most recent put value to std out
-command line tool         # response -- 0 -- printed to std out 
mr                         # FIFO is default
this is                    # response -- 0 -- printed to std out
mr -f                      # only one item left!
the mr                     # response -- 0 -- printed to std out
mr                         # exit status 1 (assuming mr started empty and had no other puts)
mr called but empty        # response -- 1 -- printed to std err
```