package example

import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.charset.StandardCharsets
import java.nio.file.{Path, Paths, StandardOpenOption}
import java.io.File

import scala.util.{Failure, Success, Try}

object Mr {
  val name = "mr"
  val usage = s"""
    Usage: $name [-h | -f | -l | data]
      -h:    display this message
      -f:    pop value from store using FIFO strategy
      -l:    pop value from store using LIFO strategy
      data:  put data into store, enclosing quotes are escaped
  """

  private def getStorePath(): Path = {
    var homeDir: String = System.getProperty("user.home")
    Paths.get(homeDir, ".mrStore")
  }

  val storePath = getStorePath()

  private def getRdLckFileChannel(path: Path): Option[FileChannel] = {
    // Append if exist or create new file (if does not exist)
    val fc = FileChannel.open(path,
                              StandardOpenOption.READ,
                              StandardOpenOption.CREATE)
    // get an exclusive lock
    Try(fc.lock()) match {
      case Success(lock) =>
        Some(fc)
      case Failure(ex) =>
        Try(fc.close())
        None
    }
  }

  private def put(data: String) = {

    getRdLckFileChannel(storePath) match {
      case Some(fc) => println("Yay")
      case None => sys.exit(1)
    }
  }

  def main(arg: String) {

    def parseOption(opt: String) = {
      def isSwitch(s: String) = (s(0) == '-')
      opt match {
        case ""                     => println(usage)
        case "-h"                   => println(usage)
        case "-f"                   => println(usage)
        case "-l"                   => println(usage)
        case data if isSwitch(data) => println(s"Unknown option $data")
        case data                   => put(data)
      }
    }

    parseOption(arg)
  }
}
