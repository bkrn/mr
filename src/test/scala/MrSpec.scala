package example

import org.scalatest._

class MrSpec extends FlatSpec with Matchers {

  "The Mr object" should "return it's usage with the help args" in {
    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      Mr.main("-h")
      stream.toString.dropRight(3) shouldEqual Mr.usage.dropRight(2)
    }
  }

  "The Mr object" should "return it's usage with no args" in {
    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      Mr.main("")
      stream.toString.dropRight(3) shouldEqual Mr.usage.dropRight(2)
    }
  }

  "The Mr object" should "complain given an unknown option" in {
    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      Mr.main("-x")
      stream.toString.dropRight(1) shouldEqual "Unknown option -x"
    }
  }

}
